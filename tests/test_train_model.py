import unittest
import os
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from cicd_example.train_model import train_model, save_model
from cicd_example.generate_prediction import load_model, predict  

class TestModel(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Load dataset and split for testing purpose
        data = load_iris()
        X, y = data.data, data.target
        cls.X_train, cls.X_test, cls.y_train, cls.y_test = train_test_split(X, y, test_size=0.2, random_state=42)
        cls.model = train_model(cls.X_train, cls.y_train)
        
    def test_train_model(self):
        self.assertEqual(len(self.model.classes_), 3)  # iris dataset

    def test_save_and_load_model(self):
        filename = 'test_model.joblib'
        save_model(self.model, filename)
        self.assertTrue(os.path.exists(filename))
        loaded_model = load_model(filename)
        os.remove(filename)  # Clean up
        self.assertEqual(len(loaded_model.classes_), 3)
        
    def test_predict(self):
        predictions = predict(self.model, self.X_test[:2])
        self.assertEqual(len(predictions), 2)  # We only predict on 2 instances

if __name__ == '__main__':
    unittest.main()
